<?php
// Check for empty fields
if(empty($_POST['name'])      ||
   empty($_POST['email'])     ||
   empty($_POST['phone'])     ||
   empty($_POST['message'])   ||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
   echo "No arguments Provided!";
   return false;
   }
   
$name = strip_tags(htmlspecialchars($_POST['name']));
$email_address = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = strip_tags(htmlspecialchars($_POST['message']));
   

$to = 'felix.schubert@schubi-lab.com'; 
$email_subject = "Eine Kontakt mail von::  $name";
$email_body = "Hier sind die Informationen über das Kontakt Formular\n\n"."Die Details:\n\nName: $name\n\nEmail: $email_address\n\nNummer: $phone\n\nNachricht:\n$message";
$headers = "From: noreply@schubi-lab.com\n"; 
$headers .= "Reply-To: $email_address";   
mail($to,$email_subject,$email_body,$headers);
return true;         
?>
