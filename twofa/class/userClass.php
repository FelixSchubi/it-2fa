<?php
class userClass
{
	 /* User Login */
     public function userLogin($usernameEmail,$password,$secret)
     {

          $db = getDB();
          $hash_password= hash('sha256', $password);
          $stmt = $db->prepare("SELECT uid FROM users WHERE  (username=:usernameEmail or email=:usernameEmail) AND  password=:hash_password");  
          $stmt->bindParam("usernameEmail", $usernameEmail,PDO::PARAM_STR) ;
          $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
          $stmt->execute();
          $count=$stmt->rowCount();
          $data=$stmt->fetch(PDO::FETCH_OBJ);
          $db = null;
          if($count)
          {
               $_SESSION['uid']=$data->uid;
               $_SESSION['google_auth_code']=$google_auth_code;
                return true;
          }
          else
          {
               return false;
          }    
     }

     /* User Registration */
     public function userRegistration($username,$password,$email,$name,$secret)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT uid FROM users WHERE username=:username OR email=:email");  
          $st->bindParam("username", $username,PDO::PARAM_STR);
          $st->bindParam("email", $email,PDO::PARAM_STR);
          $st->execute();
          $count=$st->rowCount();
          if($count<1)
          {
          $stmt = $db->prepare("INSERT INTO users(username,password,email,name,google_auth_code) VALUES (:username,:hash_password,:email,:name,:google_auth_code)");  
          $stmt->bindParam("username", $username,PDO::PARAM_STR) ;
          $hash_password= hash('sha256', $password);
          $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
          $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
          $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
          $stmt->bindParam("google_auth_code", $secret,PDO::PARAM_STR) ;
          $stmt->execute();
          $uid=$db->lastInsertId();
          $db = null;
          $_SESSION['uid']=$uid;
               // ----------------- E-Mail --------------------

               $title = null;

               $urlencoded = urlencode('otpauth://totp/'.$name.'?secret='.$secret.'');
               if(isset($title)) {
                          $urlencoded .= urlencode('&issuer='.urlencode($title));
                  }

               $qrCodeUrl = 'https://chart.googleapis.com/chart?chs=200x200&chld=M|0&cht=qr&chl='.$urlencoded.'';


               $to = $email; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
               $email_subject = "Login Code";
               // $email_body = "Hier ist ihr TOTP Code\n\n"."Code: $secret";
               $email_body = "<html><head>
               <title> Hiermit erhalten Sie ihren Code + QR-Code </title>
               </head>
               <body>
               <p>Code: <p> 
               QR-Code zum Einscannen: <br>
               <img src=\"$qrCodeUrl\">
               <p> Ihr Code: $secret<p> 
               </body>
               ";
               $headers = "From: noreply@schubi-lab.com\nMIME-Version: 1.0\nContent-Type: text/html; charset=utf-8\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
               $headers .= "Reply-To: $email";   
               mail($to,$email_subject,$email_body,$headers);



// ----------------- E-Mail --------------------
          return true;

          }
          else
          {
          $db = null;
          return false;
          }
          
         
          } 
          catch(PDOException $e) {
          echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }
     
     /* User Details */
     public function userDetails($uid)
     {
        try{
          $db = getDB();
          $stmt = $db->prepare("SELECT email,username,name,google_auth_code FROM users WHERE uid=:uid");  
          $stmt->bindParam("uid", $uid,PDO::PARAM_INT);
          $stmt->execute();
          $data = $stmt->fetch(PDO::FETCH_OBJ);
          return $data;
         }
         catch(PDOException $e) {
          echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }

     }


}
?>