<?php
session_start();


define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'db_Nutzername');
define('DB_PASSWORD', 'db_Passwort');
define('DB_DATABASE', 'db_DB-Name');
define("BASE_URL", "https://website.com/"); 


function getDB() 
{
	$dbhost=DB_SERVER;
	$dbuser=DB_USERNAME;
	$dbpass=DB_PASSWORD;
	$dbname=DB_DATABASE;
	try {
	$dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbConnection->exec("set names utf8");
	$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbConnection;
    }
    catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
	}

}
?>