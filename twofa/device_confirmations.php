<?php
include('config.php');

if(empty($_SESSION['uid']))
{
	header("Location: index.php");
}

include('class/userClass.php');
$userClass = new userClass();
$userDetails=$userClass->userDetails($_SESSION['uid']);
$secret=$userDetails->google_auth_code;
$email=$userDetails->email;

require_once 'googleLib/GoogleAuthenticator.php';

$ga = new GoogleAuthenticator();

$qrCodeUrl = $ga->getQRCodeGoogleUrl($email, $secret,'IT2FA_Schubi');


?>
<!DOCTYPE html>
<html>
<head>
<meta content='width=device-width, initial-scale=1' name='viewport'/>

    <title>IT2FA_Schubi</title>
  <!--  <link rel="stylesheet" type="text/css" href="style.css" charset="utf-8" /> -->
     <link rel="stylesheet" href="../assets/css/main.css" />

</head>
<body>
	<div id="container">
		<h1 style="text-align: -webkit-center;">IT2FA_Schubi</h1>
		<div id='device'>

<div id="device" style="max-width: 50%; margin-left: 25%;">
<p style="font-size: 120%;">Den generierten Code aus Ihrer TOTP App eingeben</p>

<form method="post" action="home.php">
<label>Hier Code eingeben</label>
<input type="text" name="code" />
<input type="submit" class="button"/>

<form action="https://schubi-lab.com">
    <input type="submit" value="Back Zur HomePage" style="margin-top: 2%;">
</form>

<!-- <form action="https://schubi-lab.com/twofa/device_confirmations.php" method="post">
    <input type="submit" name="someAction" value="GO" />
</form> -->


</form>
</div>
</div>
<div style="text-align:center">
	<h3>Den Code/QR-Code fuer Ihre TOTP App haben sie per Mail erhalten </h3>
	<p>Biite auch Spam Ordner Checken</p>
<a href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank"><img class='app' src="images/iphone.png" /></a>

<a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en" target="_blank"><img class="app" src="images/android.png" /></a> 
</div>
</div>
</body>
</html>
